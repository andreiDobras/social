# frozen_string_literal: true
class V1::PostsController < ApplicationController
  
  def index
    posts = Post.order(created_at: :ASC)
    render json: posts, status: :ok
  end
  
  def create
    post = Post.new(post_params)
    
    require 'net/http'
    json = JSON.parse Net::HTTP.get(URI.parse('https://dog.ceo/api/breeds/image/random'))
    image_url = json["message"]
    post.image_url = image_url
    
    if post.save
      render json: post, status: :created
    else
      render json: { errors: post.errors }, status:    :unprocessable_entity
    end
  end
  
  def show
    post = Post.find(params[:id])
    render json: post, status: :ok
  end
  
  def update
    post = Post.find(params[:id])
    if post.update(post_params)
      render json: post, status: :ok
    else
      render json: { errors: post.errors }, status: :unprocessable_entity
    end
  end
  
  def destroy
    post = Post.find(params[:id])
    post.destroy
    render json: post, status: :ok
  end
  
  private
  def post_params
    params.permit(:title, :message, :random_text)
  end
  
end

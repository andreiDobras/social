class V1::PostSerializer < ActiveModel::Serializer
  attributes :id, :title, :message, :image_url
end

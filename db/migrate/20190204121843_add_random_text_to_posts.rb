class AddRandomTextToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :random_text, :string
  end
end

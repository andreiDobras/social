class ChangeColumnName < ActiveRecord::Migration[5.0]
  def change
    rename_column :posts, :random_text, :image_url
  end
end
